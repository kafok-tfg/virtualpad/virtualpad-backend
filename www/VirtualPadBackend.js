var exec = require('cordova/exec')
var cordova = require('cordova')


function VirtualPad() {
}


var me = new VirtualPad()


// Optimizer

var BYTE_2_HEX = [
	'00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '0a', '0b', '0c', '0d', '0e', '0f', '10', '11', '12', '13', '14', '15', '16', '17',
	'18', '19', '1a', '1b', '1c', '1d', '1e', '1f', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '2a', '2b', '2c', '2d', '2e', '2f',
	'30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '3a', '3b', '3c', '3d', '3e', '3f', '40', '41', '42', '43', '44', '45', '46', '47',
	'48', '49', '4a', '4b', '4c', '4d', '4e', '4f', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '5a', '5b', '5c', '5d', '5e', '5f',
	'60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '6a', '6b', '6c', '6d', '6e', '6f', '70', '71', '72', '73', '74', '75', '76', '77',
	'78', '79', '7a', '7b', '7c', '7d', '7e', '7f', '80', '81', '82', '83', '84', '85', '86', '87', '88', '89', '8a', '8b', '8c', '8d', '8e', '8f',
	'90', '91', '92', '93', '94', '95', '96', '97', '98', '99', '9a', '9b', '9c', '9d', '9e', '9f', 'a0', 'a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7',
	'a8', 'a9', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', 'ba', 'bb', 'bc', 'bd', 'be', 'bf',
	'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'ca', 'cb', 'cc', 'cd', 'ce', 'cf', 'd0', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7',
	'd8', 'd9', 'da', 'db', 'dc', 'dd', 'de', 'df', 'e0', 'e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'ea', 'eb', 'ec', 'ed', 'ee', 'ef',
	'f0', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'fa', 'fb', 'fc', 'fd', 'fe', 'ff'
]


// Events

var events = []
var eventsSensor = []
var eventsFile = []
var requestsFile = []


// Cordova functions

VirtualPad.prototype.start = function (success, error) {
	exec(function (e) {
		var event = JSON.parse(e)
		if (event.type == 'start') {
			success()
			return
		}

		if (event.type == 'file-icon') {
			var buf = new ArrayBuffer(event.msg.data.length / 2)
			var bytes = new Uint8Array(buf)
			for (var i = 0; i < event.msg.data.length; i += 2)
				bytes[i / 2] = parseInt(event.msg.data.slice(i, i + 2), 16)

			event.msg.url = URL.createObjectURL(new Blob([bytes], { type: event.msg.mimeType }))
		}

		if (event.type == 'set-controller') {
			var buf = new ArrayBuffer(event.msg.data.length / 2)
			var bytes = new Uint8Array(buf)
			for (var i = 0; i < event.msg.data.length; i += 2)
				bytes[i / 2] = parseInt(event.msg.data.slice(i, i + 2), 16)

			events = []
			eventsSensor = []
			eventsFile = []
			requestsFile = []

			event.msg.code = new TextDecoder("utf-8").decode(bytes)
		}

		if (event.type == 'disconnect') {
			events = []
			eventsSensor = []
			eventsFile = []
			requestsFile = []
		}

		cordova.fireDocumentEvent(event.type, event.msg)

	}, function (e) {
		var event = JSON.parse(e)
		if (event.type == 'start')
			error()
		else
			cordova.fireDocumentEvent(event.type, event.msg)

	}, 'VirtualPadBackend', 'start', [])
}

VirtualPad.prototype.scan = function (error) {
	exec(function () { }, error, 'VirtualPadBackend', 'scan', [])
}

VirtualPad.prototype.scanDirect = function (name, ips, tcpPort, udpPort, error) {
	exec(function () { }, error, 'VirtualPadBackend', 'scan-direct', [name, ips, tcpPort, udpPort])
}

VirtualPad.prototype.connect = function (addr, port, success, error) {
	exec(success, error, 'VirtualPadBackend', 'connect', [addr, port])
}

VirtualPad.prototype.disconnect = function (success, error) {
	exec(success, error, 'VirtualPadBackend', 'disconnect', [])
}

VirtualPad.prototype.sendDataBoolean = function (idSensor, data, error) {
	exec(function() {}, error, 'VirtualPadBackend', 'data-boolean', [idSensor, data])
}

VirtualPad.prototype.sendDataInt = function (idSensor, data, error) {
	exec(function() {}, error, 'VirtualPadBackend', 'data-int', [idSensor, data])
}

VirtualPad.prototype.sendDataFloat = function (idSensor, data, error) {
	exec(function() {}, error, 'VirtualPadBackend', 'data-float', [idSensor, data])
}

VirtualPad.prototype.sendDataString = function (idSensor, data, error) {
	exec(function() {}, error, 'VirtualPadBackend', 'data-string', [idSensor, data])
}

VirtualPad.prototype.sendFile = function (name, mimeType, data, error) {
	var bytes = null
	if (data instanceof ArrayBuffer)
		bytes = new Uint8Array(data)
	else if (data instanceof Uint8Array)
		bytes = data
	else if (typeof data === 'string')
		bytes = new TextEncoder("utf-8").encode(data)
	else
		bytes = Uint8Array.from(data)

	var res = ''
	for (var i=0; i<bytes.length; i++)
		res += BYTE_2_HEX[bytes[i]]

	exec(function() {}, error, 'VirtualPadBackend', 'send-file', [name, mimeType, res])
}

VirtualPad.prototype.requestFile = function (name, callback, error) {
	if (!requestsFile[name])
		requestsFile[name] = []

	requestsFile[name].push(callback)

	exec(function() {}, error, 'VirtualPadBackend', 'request-file', [name])
}

VirtualPad.prototype.startSensors = function() {
    exec(function() {}, function() {}, 'VirtualPadBackend', 'start-sensors', [])
}

VirtualPad.prototype.stopSensors = function() {
    exec(function() {}, function() {}, 'VirtualPadBackend', 'stop-sensors', [])
}

VirtualPad.prototype.getSensors = function(success) {
    exec(function(sensors) { success(JSON.parse(sensors)) }, function() {}, 'VirtualPadBackend', 'sensors', [])
}

VirtualPad.prototype.measure = function(sensors, error) {
    exec(function() {}, error, 'VirtualPadBackend', 'measure', [sensors])
}

VirtualPad.prototype.lock = function() {
    exec(function() {}, function() {}, 'VirtualPadBackend', 'lock', [])
}

VirtualPad.prototype.unlock = function() {
    exec(function() {}, function() {}, 'VirtualPadBackend', 'unlock', [])
}

VirtualPad.prototype.readBucket = function(bucket, success, error) {
    exec(success, error, 'VirtualPadBackend', 'read-bucket', [bucket])
}

VirtualPad.prototype.deleteBucket = function(bucket, success, error) {
    exec(success, error, 'VirtualPadBackend', 'delete-bucket', [bucket])
}

VirtualPad.prototype.writeBucket = function(bucket, data, success, error) {
    exec(success, error, 'VirtualPadBackend', 'write-bucket', [bucket, data])
}

VirtualPad.prototype.listBucket = function(success, error) {
    exec(function(buckets) { success(JSON.parse(buckets)) }, error, 'VirtualPadBackend', 'list-buckets', [])
}

VirtualPad.prototype.readGlobalBucket = function(bucket, success, error) {
    exec(success, error, 'VirtualPadBackend', 'read-bucket-global', [bucket])
}

VirtualPad.prototype.deleteGlobalBucket = function(bucket, success, error) {
    exec(success, error, 'VirtualPadBackend', 'delete-bucket-global', [bucket])
}

VirtualPad.prototype.writeGlobalBucket = function(bucket, data, success, error) {
    exec(success, error, 'VirtualPadBackend', 'write-bucket-global', [bucket, data])
}

VirtualPad.prototype.listGlobalBucket = function(success, error) {
    exec(function(buckets) { success(JSON.parse(buckets)) }, error, 'VirtualPadBackend', 'list-buckets-global', [])
}

VirtualPad.prototype.listAppsData = function(success, error) {
    exec(function(apps) { success(JSON.parse(apps)) }, error, 'VirtualPadBackend', 'list-apps-data', [])
}

VirtualPad.prototype.deleteAppData = function(appName, success, error) {
    exec(success, error, 'VirtualPadBackend', 'detele-app-data', [appName])
}

VirtualPad.prototype.vibrate = function(time, success, error) {
    exec(success, error, 'VirtualPadBackend', 'vibrate', [time])
}


// Events functions

VirtualPad.prototype.receiveData = function(idSensor, data) {
	var event = events['data']
	if (event)
		event(idSensor, data)

	event = eventsSensor['' + idSensor]
	if (event)
		event(idSensor, data)
}

VirtualPad.prototype.receiveFile = function(name, mimeType, data) {
	var event = events['send-file']
	if (event)
		event(name, mimeType, data)

	event = eventsFile['' + name]
	if (event)
		event(name, mimeType, data)
}

VirtualPad.prototype.receiveRequestedFile = function(name, mimeType, data) {
	var files = requestsFile[name]
	if (files && files.length > 0) {
		var callback = files[0]

		if (files.length > 1)
			requestsFile[name] = files.slice(1)
		else
			delete requestsFile[name]

		callback(name, mimeType, data)
	}
}

function onData(listener) {
	if (listener != undefined)
		events['data'] = listener
	else
		delete events['data']
}

function onDataSensor(idSensor, listener) {
	if (listener != undefined)
		eventsSensor['' + idSensor] = listener
	else
		delete eventsSensor['' + idSensor]
}

function onReiveFile(listener) {
	if (listener != undefined)
		events['send-file'] = listener
	else
		delete events['send-file']
}

function onReiveFileName(name, listener) {
	if (listener != undefined)
		eventsFile['' + name] = listener
	else
		delete eventsFile['' + name]
}

VirtualPad.prototype.api = {
	sendDataBoolean: VirtualPad.prototype.sendDataBoolean,
	sendDataInt: VirtualPad.prototype.sendDataInt,
	sendDataFloat: VirtualPad.prototype.sendDataFloat,
	sendDataString: VirtualPad.prototype.sendDataString,
	sendFile: VirtualPad.prototype.sendFile,
	requestFile: VirtualPad.prototype.requestFile,
	startSensors: VirtualPad.prototype.startSensors,
	stopSensors: VirtualPad.prototype.stopSensors,
	getSensors: VirtualPad.prototype.getSensors,
	measure: VirtualPad.prototype.measure,
	lock: VirtualPad.prototype.lock,
	unlock: VirtualPad.prototype.unlock,
	readBucket: VirtualPad.prototype.readBucket,
	deleteBucket: VirtualPad.prototype.deleteBucket,
	writeBucket: VirtualPad.prototype.writeBucket,
	listBucket: VirtualPad.prototype.listBucket,
	readGlobalBucket: VirtualPad.prototype.readGlobalBucket,
	deleteGlobalBucket: VirtualPad.prototype.deleteGlobalBucket,
	writeGlobalBucket: VirtualPad.prototype.writeGlobalBucket,
	listGlobalBucket: VirtualPad.prototype.listGlobalBucket,

	onData: onData,
	onDataSensor: onDataSensor,

	onReiveFile: onReiveFile,
	onReiveFileName: onReiveFileName,

	Sensors: {
		ACCELEROMETER: 0x01,
		GRAVITY: 0x02,
		GYROSCOPE: 0x03,
		LIGHT: 0x04,
		LINEAR_ACCELERATION: 0x05,
		MAGNETIC_FIELD: 0x06,
		SPEAKER: 0x07,
		VIBRATION: 0x08,
		CAMERA: 0x09,
		CAMERA_FRONT: 0x0A,
		MICROPHONE: 0x0B,
		KEYBOARD: 0x0C,

		ECHO_REQ: -1,
		ECHO_RES: 0x0
	},

	RefusedErrors: {
		UNKNOW_ERROR: 0x01,
		ENOUGH_DEVICES: 0x02,
		LACK_OF_SENSORS: 0x03
	},

	util: {
		toArrayBoolean: function(size, data) {
			var res = []
			var factor = Math.floor(data.length/size)
			for(var i=0; i<data.length; i+=factor)
				res.push(data.substring(i, i+factor) == '00000001' ? true : false)

			return res
		},

		toArrayInt: function(size, data) {
			var res = []
			var factor = Math.floor(data.length/size)
			for(var i=0; i<data.length; i+=factor)
				res.push(parseInt(data.substring(i, i+factor), 16))

			return res
		},

		toArrayFloat: function(size, data) {
			var temp = []
			var factor = Math.floor(data.length/size)
			for(var i=0; i<data.length; i+=factor)
				temp.push(data.substring(i, i+factor))

			var res = []
			for(var i=0; i<temp.length; i++) {

				var dat = []
				for(var j=0; j<temp[i].length; j+=2)
					dat.push(parseInt(temp[i].substring(j, j+2), 16))

				let buf = new Uint8Array(dat)
				let view = new DataView(buf.buffer)

				res.push(view.getFloat32(0))
			}

			return res
		},

		arrayToString: function(data) {
			var temp = []
			for(var i=0; i<data.length; i+=2)
				temp.push(parseInt(data.substring(i, i+2), 16))

			var res = new Uint8Array(temp)
			return new TextDecoder('utf-8').decode(res)
		},
	}
}


module.exports = me
