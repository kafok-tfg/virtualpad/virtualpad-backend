package virtualpad.backend;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.webkit.WebView;

import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VirtualPadBackend extends CordovaPlugin {

	private Protocol protocol;

	private PowerManager powerManager;
	private PowerManager.WakeLock wakeLock;
	private WindowManager windowManager;

	private Vibrator vibrator;

	private float tempBrightness;
	private boolean dirtyBrightness;


	private static final String[] BYTE_2_HEX = {
		"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "10", "11", "12", "13", "14", "15", "16", "17",
		"18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f",
		"30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "40", "41", "42", "43", "44", "45", "46", "47",
		"48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f",
		"60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", "70", "71", "72", "73", "74", "75", "76", "77",
		"78", "79", "7a", "7b", "7c", "7d", "7e", "7f", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f",
		"90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7",
		"a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf",
		"c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7",
		"d8", "d9", "da", "db", "dc", "dd", "de", "df", "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef",
		"f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff"
	};


	public VirtualPadBackend() {
		super();
	}


	// Cordova

	private CallbackContext callback;

	private void fireEvent(String event, String msg) {
		if(callback != null) {
			PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "{\"type\": \"" + event + "\", \"msg\": " + msg +"}");
			pluginResult.setKeepCallback(true);
			callback.sendPluginResult(pluginResult);
		}
	}

	private String toHex(byte[] data) {
		StringBuilder res = new StringBuilder();

		int i = 0;
		if(data != null) {
			res.setLength(data.length*2);
			for(byte b : data) {
				String oct = BYTE_2_HEX[b < 0 ? b + 256 : b];
				res.setCharAt(i, oct.charAt(0));
				res.setCharAt(i+1, oct.charAt(1));

				i+=2;
			}
		}

		return res.toString();
	}

	private byte[] hexToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

	@Override
	public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {

		switch(action) {

			// Life-cicle App
			case "start":
				start(callbackContext);
				break;


			// Connection
			case "scan":
				scan(callbackContext);
				break;

			case "scan-direct":
				scanDirect(args, callbackContext);
				break;

			case "connect":
				connect(args.getString(0), args.getInt(1), callbackContext);
				break;

			case "disconnect":
				disconnect(callbackContext);
				break;


			// Data
			case "data-boolean":
				sendDataBoolean(args, callbackContext);
				break;

			case "data-int":
				sendDataInt(args, callbackContext);
				break;

			case "data-float":
				sendDataFloat(args, callbackContext);
				break;

			case "data-string":
				sendDataString(args, callbackContext);
				break;

			case "send-file":
				sendFile(args, callbackContext);
				break;


			// Request-File
			case "request-file":
				requestFile(args, callbackContext);
				break;


			// Sensors
			case "start-sensors":
				protocol.getSensorComponent().start();
				break;

			case "stop-sensors":
				protocol.getSensorComponent().stop();
				break;

			case "measure":
				measure(args, callbackContext);
				break;

			case "sensors":
				getSensors(args, callbackContext);
				break;


			// Energy
			case "lock":
				lock(callbackContext);
				break;

			case "unlock":
				unlock(callbackContext);
				break;


			// Buckets storage
			case "read-bucket":
				readBucket(args, callbackContext, false);
				break;

			case "delete-bucket":
				deleteBucket(args, callbackContext, false);
				break;

			case "write-bucket":
				writeBucket(args, callbackContext, false);
				break;

			case "list-buckets":
				listBuckets(args, callbackContext, false);
				break;


			case "read-bucket-global":
				readBucket(args, callbackContext, true);
				break;

			case "delete-bucket-global":
				deleteBucket(args, callbackContext, true);
				break;

			case "write-bucket-global":
				writeBucket(args, callbackContext, true);
				break;

			case "list-buckets-global":
				listBuckets(args, callbackContext, true);
				break;


			// Manage data
			case "list-apps-data":
				listAppsData(args, callbackContext);
				break;

			case "detele-app-data":
				deleteAppData(args, callbackContext);
				break;


			// Vibration
			case "vibrate":
				vibrate(args, callbackContext);
				break;


			// Invalid
			default:
				return false;
		}

		return true;
	}

	private void start(CallbackContext callbackContext) {
		try {
			Window window = this.cordova.getActivity().getWindow();
			WindowManager.LayoutParams params = window.getAttributes();
			tempBrightness = params.screenBrightness;

			powerManager = (PowerManager) this.cordova.getActivity().getSystemService(Context.POWER_SERVICE);
			windowManager = (WindowManager) this.cordova.getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

			vibrator = (Vibrator) this.cordova.getActivity().getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

			protocol = new Protocol(this);
			protocol.getNetwork().start();
			this.callback = callbackContext;

			this.cordova.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					try {
						WebView.setWebContentsDebuggingEnabled(true);
					} catch (Exception e) {
						log(e);
					}
				}

			});
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "{\"type\": \"start\"}");
		pluginResult.setKeepCallback(true);
		callback.sendPluginResult(pluginResult);
	}

	public void scan(CallbackContext callbackContext) {
		try {
			protocol.scan(null);
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void scanDirect(CordovaArgs args, CallbackContext callbackContext) {
		try {
			JSONArray ips = args.getJSONArray(1);
			for (int i=0; i<ips.length(); i++)
				protocol.scan(ips.getString(i));
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void connect(String addr, int port, CallbackContext callbackContext) {
		new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					protocol.connect(addr, port);
				} catch (Exception e) {
					callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
					log(e);
					return;
				}

				callbackContext.success();
			}
		}).start();
	}

	public void disconnect(CallbackContext callbackContext) {
		try {
			vibrator = null;
			protocol.getNetwork().closeTCP();
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void sendDataBoolean(CordovaArgs args, CallbackContext callbackContext) {
		try {
			int idSensor = args.getInt(0);
			JSONArray params = args.getJSONArray(1);

			boolean[] data = new boolean[params.length()];
			for(int i=0; i<params.length(); i++)
				data[i] = params.getBoolean(i);

			protocol.sendData(idSensor, data);
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void sendDataInt(CordovaArgs args, CallbackContext callbackContext) {
		try {
			int idSensor = args.getInt(0);
			JSONArray params = args.getJSONArray(1);

			int[] data = new int[params.length()];
			for(int i=0; i<params.length(); i++)
				data[i] = params.getInt(i);

			protocol.sendData(idSensor, data);
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void sendDataFloat(CordovaArgs args, CallbackContext callbackContext) {
		try {
			int idSensor = args.getInt(0);
			JSONArray params = args.getJSONArray(1);

			float[] data = new float[params.length()];
			for(int i=0; i<params.length(); i++)
				data[i] = ((Double) params.getDouble(i)).floatValue();

			protocol.sendData(idSensor, data);
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void sendDataString(CordovaArgs args, CallbackContext callbackContext) {
		try {
			protocol.sendData(args.getInt(0), args.getString(1));
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void requestFile(CordovaArgs args, CallbackContext callbackContext) {
		try {
			protocol.requestFile(args.getString(0));
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void sendFile(CordovaArgs args, CallbackContext callbackContext) {
		try {
			protocol.sendFile(args.getString(0), args.getString(1), hexToByteArray(args.getString(2)));
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void measure(CordovaArgs args, CallbackContext callbackContext) {
		try {
			JSONArray sensors = args.getJSONArray(0);
			List<Integer> res = new LinkedList<>();
			for(int i=0; i<sensors.length(); i++)
				res.add(sensors.getInt(i));

			protocol.getSensorComponent().measure(res, this);
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}

		callbackContext.success();
	}

	public void getSensors(CordovaArgs args, CallbackContext callbackContext) {
		try {
			String res = "";
			for(int sensor : protocol.getSensorComponent().getSensors())
				res += ", " + sensor;

			if(res.length() > 0)
				res = res.substring(2);

			callbackContext.success("{\"sensors\": [" + res + "]}");
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}


	private long startedLockUsed;
	private long lockUsedTime;

	public void lock(CallbackContext callbackContext) {
		VirtualPadBackend _this = this;
		this.cordova.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {

				Window window = _this.cordova.getActivity().getWindow();
				WindowManager.LayoutParams params = window.getAttributes();
				if(!dirtyBrightness) {
					tempBrightness = params.screenBrightness;
					dirtyBrightness = true;
				}
				params.screenBrightness = 0;
				window.setAttributes(params);

				if(startedLockUsed < 0)
					startedLockUsed = System.currentTimeMillis();

				callbackContext.success();
			}

		});
	}

	public void unlock(CallbackContext callbackContext) {
		VirtualPadBackend _this = this;
		this.cordova.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {

				Window window = _this.cordova.getActivity().getWindow();
				WindowManager.LayoutParams params = window.getAttributes();
				params.screenBrightness = tempBrightness;
				dirtyBrightness = false;
				window.setAttributes(params);

				if(startedLockUsed > 0) {
					lockUsedTime += (System.currentTimeMillis() - startedLockUsed);
					startedLockUsed = -1;
				}

				if(callbackContext != null)
					callbackContext.success();
			}

		});
	}

	public void readBucket(CordovaArgs args, CallbackContext callbackContext, boolean isGlobal) {
		try {
			String appName = protocol.getHost().getName();
			String bucket = args.getString(0);

			checkName("Bucket", bucket);

			String res = StorageComponent.readBucket(this.cordova.getActivity().getApplicationContext(), appName, bucket, isGlobal);

			callbackContext.success(res);

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void deleteBucket(CordovaArgs args, CallbackContext callbackContext, boolean isGlobal) {
		try {
			String appName = protocol.getHost().getName();
			String bucket = args.getString(0);

			checkName("Bucket", bucket);

			StorageComponent.deleteBucket(this.cordova.getActivity().getApplicationContext(), appName, bucket, isGlobal);

			callbackContext.success();

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void writeBucket(CordovaArgs args, CallbackContext callbackContext, boolean isGlobal) {
		try {
			String appName = protocol.getHost().getName();
			String bucket = args.getString(0);
			String data = args.getString(1);

			checkName("Application", appName);
			checkName("Bucket", bucket);

			StorageComponent.writeBucket(this.cordova.getActivity().getApplicationContext(), protocol.getHost(), appName, bucket, data, isGlobal);

			callbackContext.success();

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void listBuckets(CordovaArgs args, CallbackContext callbackContext, boolean isGlobal) {
		try {
			String appName = protocol.getHost().getName();

			String list[] = StorageComponent.listBuckets(this.cordova.getActivity().getApplicationContext(), appName, isGlobal);
			StringBuilder sb = new StringBuilder();
			if(list == null) {
				list = new String[0];
				sb.append(",");
			} else if(list.length == 0)
				sb.append(",");

			for(String bucket : list)
				sb.append("\"" + bucket + "\",");

			sb.setLength(sb.length()-1);

			callbackContext.success("{\"buckets\": [" + sb.toString() + "]}");

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void listAppsData(CordovaArgs args, CallbackContext callbackContext) {
		try {
			String json = "[";
			for(Map<String, Object> app : StorageComponent.getDataInformation(this.cordova.getActivity().getApplicationContext())) {
				json += "{";

				if(app.get("icon") != null)
					json += "\"icon\": {\"data\": \"" + toHex((byte[]) app.get("icon")) + "\", \"mimeType\": \"" + app.get("iconMimeType") + "\"},";

				json += "\"name\": \"" + app.get("name") + "\",";
				json += "\"size\": \"" + app.get("size") + "\",";
				json += "\"measure\": \"" + app.get("measure") + "\",";
				json += "\"lastModified\": " + app.get("lastModified");

				json += "},";
			}

			if (json.indexOf(",") >= 0)
				json = json.substring(0, json.length()-1);

			json += "]";

			callbackContext.success(json);

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void deleteAppData(CordovaArgs args, CallbackContext callbackContext) {
		try {
			String appName = args.getString(0);

			checkName("Application", appName);

			StorageComponent.deleteData(this.cordova.getActivity().getApplicationContext(), appName);

			callbackContext.success();

		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	public void vibrate(CordovaArgs args, CallbackContext callbackContext) {
		try {
			int time = args.getInt(0);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				vibrator.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
			} else { // deprecated in API 26
				vibrator.vibrate(time);
			}
		} catch (Exception e) {
			callbackContext.error("{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
			log(e);
		}
	}

	private void checkName(String name, String check) {
		if(check == null || check.isEmpty())
			throw new IllegalArgumentException(name + " name must not empty");

		Pattern p = Pattern.compile("^[\\w. -]*$");
		Matcher m = p.matcher(check);

		if(!m.matches())
			throw new IllegalArgumentException(name + " name can only contain numbers, letters, whitespaces, '-' and '.', it was given '" + check + "'.");
	}


	// Events

	public void onEcho(Host host) throws IOException {
		fireEvent("echo", host.toString());
	}

	public void onConnect(Host host) {
		wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "VirtualPad");
		wakeLock.acquire();

		lockUsedTime = 0;
		startedLockUsed = -1;

		fireEvent("connect", host.toString());
	}

	public void onConnectionRefused(int cause, List<Integer> sensors) {
		if(sensors != null && sensors.size() > 0) {
			StringBuilder res = new StringBuilder();
			for(Integer s : sensors)
				res.append(", " + s);

			fireEvent("connection-refused", "{\"cause\": " + cause + ", \"sensors\": [" + res.substring(2).toString() + "]}");
		} else {
			fireEvent("connection-refused", "{\"cause\": " + cause + "}");
		}
	}

	public void onDisconnect(Host host, String statistics) {
		try {
			wakeLock.release();
		} catch (Throwable th) {}

		unlock(null);

		// Warn of disconection
		if(vibrator == null)	// User disconnect
			vibrator = (Vibrator) this.cordova.getActivity().getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
		else {	// Server disconnect
			int time = 80;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				vibrator.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
				try {
					Thread.sleep(time*2);
				} catch(Exception e) {}
				vibrator.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
			} else { // deprecated in API 26
				vibrator.vibrate(time);
				try {
					Thread.sleep(time*2);
				} catch(Exception e) {}
				vibrator.vibrate(time);
			}
		}

		statistics = statistics.replace("LOCK_USED", ""+lockUsedTime);
		if(host != null)
			fireEvent("disconnect", "{\"host\":" + host.toString() + ", \"statistics\":" + statistics + "}");
	}

	public void onData(int idSensor, byte[] data) {
		fireEvent("data", "{\"idSensor\": " + idSensor + ", \"data\": \"" + toHex(data) + "\"}");
	}

	public void onFile(String name, String mimeType, byte[] data) {
		fireEvent("file", "{"
			+ "\"name\": \"" + name + "\""
			+ ", \"mimeType\": \"" + mimeType + "\""
			+ ", \"data\": \"" + toHex(data) + "\""
		+ "}");
	}

	public void onRequestSetController() {
		fireEvent("request-set-controller", "{}");
	}

	public void onSendFile(String name, String mimeType, byte[] data) {
		fireEvent("send-file", "{"
			+ "\"name\": \"" + name + "\""
			+ ", \"mimeType\": \"" + mimeType + "\""
			+ ", \"data\": \"" + toHex(data) + "\""
		+ "}");
	}

	public void onSetController(byte[] data, String name) throws UnsupportedEncodingException {
		protocol.getHost().setName(name);
		protocol.getHost().setIcon(null);
		protocol.getHost().setIconMimeType(null);

		try {
			protocol.requestFile("icon");
		} catch (IOException e) {}

		fireEvent("set-controller", "{ \"data\": \"" + toHex(data) + "\" }");
	}

	public void onFileIcon(String addr, int port, String mimeType, byte[] data) {
		fireEvent("file-icon", "{"
			+ "\"addr\": \"" + addr + "\""
			+ ", \"port\": " + port
			+ ", \"mimeType\": \"" + mimeType + "\""
			+ ", \"data\": \"" + toHex(data) + "\""
		+ "}");
	}

	public void onError(Exception e) {
		log(e);
		fireEvent("error", "{\"msg\": \"" + e.getLocalizedMessage() + "\"}");
	}

	public void onTimeoutError() {
		System.out.println("Timeout!!");
		fireEvent("timeout", "{}");
	}


	// Logs

	public static void log(String msg) {
		System.out.println(msg);
	}

	public static void log(Throwable e) {
		e.printStackTrace();
	}

}
