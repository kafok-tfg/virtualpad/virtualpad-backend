package virtualpad.backend.constants;

public class Messages {

	public static final int SCAN = 0x01;
	public static final int ECHO = 0x02;
	public static final int CONNECTION_REQUEST = 0x03;
	public static final int CONNECTION_SUCCESSFUL_RESPONSE = 0x04;
	public static final int CONNECTION_REFUSED_RESPONSE = 0x05;
	public static final int HEARTBEAT = 0x06;
	public static final int DATA = 0x07;
	public static final int REQUEST_FILE = 0x08;
	public static final int RESPONSE_FILE = 0x09;
	public static final int REQUEST_SET_CONTROLER = 0x0A;
	public static final int SEND_FILE = 0x0B;
	
}
