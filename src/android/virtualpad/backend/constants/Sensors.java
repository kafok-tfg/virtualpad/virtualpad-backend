package virtualpad.backend.constants;

public class Sensors {

	public static final int ACCELEROMETER = 0x01;
	public static final int GRAVITY = 0x02;
	public static final int GYROSCOPE = 0x03;
	public static final int LIGHT = 0x04;
	public static final int LINEAR_ACCELERATION = 0x05;
	public static final int MAGNETIC_FIELD = 0x06;
	public static final int SPEAKER = 0x07;
	public static final int VIBRATION = 0x08;
	public static final int CAMERA = 0x09;
	public static final int CAMERA_FRONT = 0x0A;
	public static final int MICROPHONE = 0x0B;
	public static final int KEYBOARD = 0x0C;
	
	public static final int ECHO_REQ = -1;
	public static final int ECHO_RES = 0x0;
	
}
