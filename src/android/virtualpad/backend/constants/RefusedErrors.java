package virtualpad.backend.constants;

public class RefusedErrors {

	public static final int UNKNOW_ERROR = 0x01;
	public static final int ENOUGH_DEVICES = 0x02;
	public static final int LACK_OF_SENSORS = 0x03;
	
}
