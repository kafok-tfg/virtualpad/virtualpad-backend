package virtualpad.backend;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

import virtualpad.backend.constants.Messages;
import virtualpad.backend.constants.RefusedErrors;
import virtualpad.backend.constants.Sensors;

public class Protocol {

	// Attributes

	private VirtualPadBackend plugin;
	private Network network;

	private SensorComponent sensorComponent;


	// Constructor

	public Protocol(VirtualPadBackend plugin) {
		super();
		this.plugin = plugin;
		this.network = new Network(this);
		this.hosts = new HashMap<String, Host>();
		this.lastHost = null;

		this.sensorComponent = new SensorComponent(this);
	}

	public Network getNetwork() {
		return network;
	}

	public VirtualPadBackend getPlugin() {
		return plugin;
	}

	public SensorComponent getSensorComponent() {
		return sensorComponent;
	}


	// Application data

	private Map<String, Host> hosts;
	private Host host;
	private Host lastHost;

	public void remove(String addr) {
		hosts.remove(addr);
	}

	public List<Host> getHosts() {
		return new ArrayList<Host>(hosts.values());
	}

	public void clearHosts() {
		hosts.clear();
	}

	public boolean isConnected() {
		return host != null;
	}

	public Host getHost() {
		return host;
	}

	private void connectHost(String addr, int tcpPort) {
		host = hosts.get(addr + ":" + tcpPort);
		if(host == null)
			throw new IllegalStateException("The host '" + addr + "' was removed!");

		lastHost = host;
	}


	// Send

	public void scan(String addr) throws SocketException, IOException {

		if (lastHost != null)
			plugin.onEcho(lastHost);

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(20);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.SCAN);					// Type
		output.writeInt(20);							// Size
		output.writeInt(network.getServerPortUDP());	// UDP Server Port
		output.writeLong(System.currentTimeMillis());	// Timestamp


		// Send

		int[] ports = {42000, 43000, 44000, 45000, 46000, 47000, 48000};
		for(int port : ports)
			if (addr == null)
				for(InetAddress address : network.getBroadcastAddress())
					network.sendBroadcastUDP(bytes.toByteArray(), address, port);
			else
				network.sendBroadcastUDP(bytes.toByteArray(), InetAddress.getByName(addr), port);
	}


	public void connect(String host, int port) throws IOException {
		network.closeTCP();
		network.openTCP(host, port);

		// Create message

		List<Integer> sensors = sensorComponent.getSensors();

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(sensors.size()*4 + 20);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.CONNECTION_REQUEST);	// Type
		output.writeInt(sensors.size()*4 + 20);			// Size
		output.writeInt(Network.TIMEOUT);				// Timeout
		output.writeInt(network.getServerPortUDP());	// UDP Port

		output.writeInt(sensors.size());
		for(int sensor : sensors)	// Sensors list
			output.writeInt(sensor);


		// Send

		network.sendTCP(bytes.toByteArray());
	}


	public void sendData(int idSensor, float... data) throws IOException {

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(Math.min(12 + data.length*4, 512));
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.DATA);							// Type
		output.writeInt(Math.min(12 + data.length*4, 512));		// Size
		output.writeInt(idSensor);								// ID Sensor

		for(float dat : data)									// Value
			output.writeFloat(dat);


		// Send

		if(host != null)
			network.sendUDP(bytes.toByteArray(), host.getAddr(), host.getUdpPort());
	}

	public void sendData(int idSensor, int... data) throws IOException {

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(Math.min(12 + data.length*4, 512));
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.DATA);							// Type
		output.writeInt(Math.min(12 + data.length*4, 512));		// Size
		output.writeInt(idSensor);								// ID Sensor

		for(int dat : data)										// Value
			output.writeInt(dat);


		// Send

		if(host != null)
			network.sendUDP(bytes.toByteArray(), host.getAddr(), host.getUdpPort());
	}

	public void sendData(int idSensor, boolean... data) throws IOException {

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(Math.min(12 + data.length*4, 512));
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.DATA);							// Type
		output.writeInt(Math.min(12 + data.length*4, 512));		// Size
		output.writeInt(idSensor);								// ID Sensor

		for(boolean dat : data)									// Value
			output.writeInt(dat ? 1 : 0);


		// Send

		if(host != null)
			network.sendUDP(bytes.toByteArray(), host.getAddr(), host.getUdpPort());
	}

	public void sendData(int idSensor, String data) throws IOException {

		// Create message

		byte[] text = data.getBytes("UTF-8");

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(Math.min(12 + text.length, 512));
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.DATA);							// Type
		output.writeInt(Math.min(12 + text.length, 512));		// Size
		output.writeInt(idSensor);								// ID Sensor
		output.write(text, 0, Math.min(text.length, 500));		// Value


		// Send

		if(host != null)
			network.sendUDP(bytes.toByteArray(), host.getAddr(), host.getUdpPort());
	}

	private void sendData(int idSensor, byte[] data) throws IOException {

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(Math.min(12 + data.length, 512));
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.DATA);						// Type
		output.writeInt(Math.min(12 + data.length, 512));	// Size
		output.writeInt(idSensor);							// ID Sensor
		output.write(data, 0, Math.min(data.length, 500));	// Value


		// Send

		if(host != null)
			network.sendUDP(bytes.toByteArray(), host.getAddr(), host.getUdpPort());
	}


	public void heartbeat() throws IOException {

		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(8);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.HEARTBEAT);			// Type
		output.writeInt(8);								// Size


		// Send

		network.sendTCP(bytes.toByteArray());
	}


	public void requestFile(String name) throws IOException {

		byte[] dataName = name.getBytes("UTF-8");


		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(12 + dataName.length);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.REQUEST_FILE);			// Type
		output.writeInt(12 + dataName.length);			// Size
		output.writeInt(dataName.length);				// Name length
		output.write(dataName);							// Name


		// Send

		network.sendTCP(bytes.toByteArray());

	}

	public void requestIconFile(String host, int port) throws IOException {

		byte[] dataName = "icon".getBytes("UTF-8");


		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(12 + dataName.length);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.REQUEST_FILE);			// Type
		output.writeInt(12 + dataName.length);			// Size
		output.writeInt(dataName.length);				// Name length
		output.write(dataName);							// Name


		// Send

		network.sendSingleTCP(host, port, bytes.toByteArray());
	}

	public void sendFile(String name, String mimeType, byte[] file) throws IOException {

		byte[] dataName = name.getBytes("UTF-8");
		byte[] dataMimeType = mimeType.getBytes("UTF-8");


		// Create message

		ByteArrayOutputStream bytes = new ByteArrayOutputStream(20 + dataName.length + dataMimeType.length + file.length);
		DataOutputStream output = new DataOutputStream(bytes);
		output.writeInt(Messages.SEND_FILE);												// Type
		output.writeInt(20 + dataName.length + dataMimeType.length + file.length);			// Size
		output.writeInt(dataName.length);													// Name length
		output.write(dataName);																// Name
		output.writeInt(dataMimeType.length);												// MIME Type length
		output.write(dataMimeType);															// MIME Type
		output.writeInt(file.length);														// File length
		output.write(file);																	// File


		// Send

		network.sendTCP(bytes.toByteArray());
	}


	// Events

	public void onDisconnect() {
		sensorComponent.stop();
		String statistics = sensorComponent.stopSession();

		plugin.onDisconnect(host, statistics);
		this.host = null;
	}


	// Received

	public Runnable receivedUDP(InetAddress addr, int port, byte[] data) {
		return new Runnable() {

			@Override
			public void run() {

				try {
					ByteBuffer buffer = ByteBuffer.wrap(data);
					int type = buffer.getInt();
					int size = buffer.getInt();
					switch(type) {

						case Messages.ECHO:
							echo(addr, buffer);
							break;

						case Messages.DATA:
							data(addr, buffer, size);
							break;

					}

				} catch(IOException e) {
					plugin.onError(e);
				}

			}

		};
	}

	public Runnable receivedTCP(InetAddress addr, int port, int type, byte[] data) {
		return new Runnable() {

			@Override
			public void run() {

				try {
					ByteBuffer buffer = ByteBuffer.wrap(data);

					switch(type) {

						case Messages.CONNECTION_SUCCESSFUL_RESPONSE:
							connectionSuccessfulResponse(addr, port, type, buffer);
							break;

						case Messages.CONNECTION_REFUSED_RESPONSE:
							connectionRefusedResponse(addr, port, type, buffer);
							break;

						case Messages.HEARTBEAT:
							break;

						case Messages.RESPONSE_FILE:
							responseFile(addr, port, type, buffer);
							break;

						case Messages.SEND_FILE:
							sendFile(addr, port, type, buffer);
							break;

						case Messages.REQUEST_SET_CONTROLER:
							requestSetController(addr, port, type, buffer);
							break;

					}

				} catch(RejectedExecutionException e) {
					// Nothing here
				} catch(Exception e) {
					plugin.onError(e);
				}

			}

		};
	}

	private String readString(ByteBuffer buffer) throws UnsupportedEncodingException {
		byte[] data = readBinary(buffer);
		return data == null || data.length == 0 ? "" : new String(data, "UTF-8");
	}

	private byte[] readBinary(ByteBuffer buffer) {
		int length = buffer.getInt();

		byte[] data = null;

		if(length > 0) {
			data = new byte[length];
			buffer.get(data);
		}

		return data;
	}


	private void echo(InetAddress addr, ByteBuffer buffer) throws IOException {
		int udpPort = buffer.getInt();
		int tcpPort = buffer.getInt();
		String name = readString(buffer);
		long timestamp = buffer.getLong();

		Host host = new Host();
		host.setAddr(addr.getHostAddress());
		host.setUdpPort(udpPort);
		host.setTcpPort(tcpPort);
		host.setName(name);
		host.setLatency(Math.min(10000, Math.max(1, (int) (System.currentTimeMillis() - timestamp))));

		if (lastHost != null)
			hosts.put(lastHost.getAddr() + ":" + lastHost.getTcpPort(), lastHost);

		hosts.put(host.getAddr() + ":" + host.getTcpPort(), host);
		requestIconFile(host.getAddr(), host.getTcpPort());

		if(this.host != null && this.host.getAddr().equals(host.getAddr())) {
			this.host = host;
			requestFile("icon");
		}

		plugin.onEcho(host);
	}

	private void data(InetAddress addr, ByteBuffer buffer, int size) {
		int sensor = buffer.getInt();

		if(sensor == Sensors.ECHO_REQ) {
			byte[] data = new byte[size - 12];
			buffer.get(data);
			try {
				sendData(Sensors.ECHO_RES, data);
			} catch(Exception e) {}
		} else if(!sensorComponent.onData(sensor, buffer)) {
			byte[] data = new byte[size - 12];
			buffer.get(data);

			plugin.onData(sensor, data);
		}
	}

	private void connectionSuccessfulResponse(InetAddress addr, int port, int type, ByteBuffer buffer) {
		int timeout = buffer.getInt();
		connectHost(addr.getHostAddress(), port);
		network.startHeartbeat(timeout);

		sensorComponent.startSession();

		plugin.onConnect(host);
	}

	private void connectionRefusedResponse(InetAddress addr, int port, int type, ByteBuffer buffer) {
		int cause = buffer.getInt();

		List<Integer> sensors = new LinkedList<Integer>();
		if(cause == RefusedErrors.LACK_OF_SENSORS) {

			int size = buffer.getInt();
			for(int i=0; i<size; i++)
				sensors.add(buffer.getInt());

		}

		plugin.onConnectionRefused(cause, sensors);

		network.closeTCP();
	}

	private void responseFile(InetAddress addr, int port, int type, ByteBuffer buffer) throws UnsupportedEncodingException {
		String name = readString(buffer);
		String mimeType = readString(buffer);
		byte[] file = readBinary(buffer);

		switch(name) {
			case "icon":
				Host host = this.hosts.get(addr.getHostAddress() + ":" + port);
				if(host != null && file.length > 0) {
					host.setIcon(file);
					host.setIconMimeType(mimeType);
				}

				plugin.onFileIcon(addr.getHostAddress(), port, mimeType, file);
				break;

			case "controller":
				plugin.onSetController(file, mimeType);
				break;

			default:
				plugin.onFile(name, mimeType, file);
				break;
		}
	}

	private void sendFile(InetAddress addr, int port, int type, ByteBuffer buffer) throws UnsupportedEncodingException {
		String name = readString(buffer);
		String mimeType = readString(buffer);
		byte[] file = readBinary(buffer);

		plugin.onSendFile(name, mimeType, file);
	}

	private void requestSetController(InetAddress addr, int port, int type, ByteBuffer buffer) throws IOException {
		requestFile("controller");
		plugin.onRequestSetController();
	}
}
