package virtualpad.backend;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Network implements Closeable {

	public static final int TIMEOUT = 12000;

	// Attributes

	private Protocol protocol;

	private DatagramSocket socketUDP;
	private Socket socket;
	private ExecutorService executorListenerUDP;
	private ExecutorService executorListenerTCP;
	private ExecutorService executorSender;
	private Thread heartbeatThread;


	// Constructor

	public Network(Protocol protocol) {
		super();
		this.protocol = protocol;
	}


	// Methods

	public void start() throws SocketException {
		socketUDP = new DatagramSocket();
		executorListenerUDP = Executors.newSingleThreadExecutor();
		executorListenerTCP = Executors.newSingleThreadExecutor();
		executorSender = Executors.newSingleThreadExecutor();


		// UDP Server

		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					while(!socketUDP.isClosed()) {
						byte[] data = new byte[516];
						DatagramPacket packet = new DatagramPacket(data, data.length);
						socketUDP.receive(packet);

						executorListenerUDP.execute(protocol.receivedUDP(packet.getAddress(), packet.getPort(), data));
					}

				} catch(SocketException e) {
					// Socket closed
				} catch (Exception e) {
					protocol.getPlugin().onError(e);
				}
			}

		}).start();

	}

	public void sendUDP(byte[] data, String host, int port) {
		executorSender.execute(new Runnable() {

			@Override
			public void run() {
				try {
					socketUDP.send(new DatagramPacket(data, data.length, InetAddress.getByName(host), port));
				} catch (IOException e) {
					VirtualPadBackend.log(e);
				}
			}

		});
	}

	public void sendBroadcastUDP(byte[] data, InetAddress host, int port) throws IOException {
		DatagramSocket socket = new DatagramSocket();
		socket.setBroadcast(true);
		socket.send(new DatagramPacket(data, data.length, host, port));
		socket.close();
	}

	public void openTCP(String host, int port) throws IOException {
		if(socket != null && !socket.isClosed())
			socket.close();

		Socket socket = new Socket(host, port);
		socket.setKeepAlive(true);
		socket.setSoTimeout(Network.TIMEOUT);

		if(this.socket == null  || this.socket.isClosed())
			this.socket = socket;
		else {
			socket.close();
			return;
		}

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					byte[] read = new byte[8];
					int len = 0;
					while((len = socket.getInputStream().read(read, 0, read.length)) >= 0) {

						ByteArrayOutputStream data = new ByteArrayOutputStream();
						ByteBuffer buffer = ByteBuffer.wrap(read);
						int type = buffer.getInt();
						int size = buffer.getInt();

						if(size > 8) {
							int count = size - 8;
							read = new byte[count > 1024 ? 1024 : count];
							while(count > 0) {
								len = socket.getInputStream().read(read, 0, read.length);
								data.write(read, 0, len);
								if(count <= 1024)
									read = new byte[count];

								count -= len;
							}
						}

						executorListenerTCP.execute(protocol.receivedTCP(socket.getInetAddress(), socket.getPort(), type, data.toByteArray()));
						read = new byte[8];
					}


				} catch(SocketException e) {
					// Socket closed
				} catch(SocketTimeoutException e) {
					protocol.getPlugin().onTimeoutError();
				} catch(IOException e) {
					protocol.getPlugin().onError(e);
				} catch(Exception e) {
					protocol.getPlugin().onError(e);
				} finally {
					if(heartbeatThread != null && heartbeatThread.isAlive())
						heartbeatThread.interrupt();

					protocol.onDisconnect();

					try {
						socket.close();
					} catch (Exception e) {} // The host could close the socket before

					executorListenerTCP.shutdownNow();
					executorSender.shutdownNow();
					executorListenerTCP = Executors.newSingleThreadExecutor();
					executorSender = Executors.newSingleThreadExecutor();
				}
			}

		}).start();
	}

	public void sendSingleTCP(String host, int port, byte[] data) throws UnknownHostException, IOException {
		Socket socket = new Socket(host, port);
		socket.setKeepAlive(false);
		socket.setSoTimeout(Network.TIMEOUT);

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					socket.getOutputStream().write(data);
					socket.getOutputStream().flush();

					byte[] read = new byte[8];
					int len = 0;
					while((len = socket.getInputStream().read(read, 0, read.length)) >= 0) {

						ByteArrayOutputStream data = new ByteArrayOutputStream();
						ByteBuffer buffer = ByteBuffer.wrap(read);
						int type = buffer.getInt();
						int size = buffer.getInt();

						if(size > 8) {
							int count = size - 8;
							read = new byte[count > 1024 ? 1024 : count];
							while(count > 0) {
								len = socket.getInputStream().read(read, 0, read.length);
								data.write(read, 0, len);
								if(count <= 1024)
									read = new byte[count];

								count -= len;
							}
						}

						executorListenerTCP.execute(protocol.receivedTCP(socket.getInetAddress(), socket.getPort(), type, data.toByteArray()));
						break;
					}


				} catch(SocketException e) {
					// Socket closed
				} catch(SocketTimeoutException e) {
					// Nothing happens here
				} catch(Exception e) {
					protocol.getPlugin().onError(e);
				} finally {
					try {
						socket.close();
					} catch (Exception e) {} // The host could close the socket before
				}
			}

		}).start();
	}

	public void closeTCP() {
		try {
			socket.close();
		} catch (Exception e) {}	// The host could close the socket before
	}

	public void sendTCP(byte[] data) {
		if(socket != null && !socket.isClosed()) {

			executorSender.execute(new Runnable() {

				@Override
				public void run() {
					try {
						socket.getOutputStream().write(data);
						socket.getOutputStream().flush();
					} catch(SocketException e) {
						// Socket closed
					} catch (Exception e) {
						protocol.getPlugin().onError(e);
					}
				}

			});

		} else
			throw new IllegalStateException("The connection does not exist");
	}

	public void startHeartbeat(int timeout) {
		heartbeatThread = new Thread(new HeartbeatTask(timeout, protocol));
		heartbeatThread.start();
	}


	// Info

	public List<InetAddress> getBroadcastAddress() throws SocketException {
		List<InetAddress> res = new LinkedList<InetAddress>();

		Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		while(interfaces.hasMoreElements()) {
			NetworkInterface networkInterface = interfaces.nextElement();
			if(networkInterface.isLoopback())
				continue;

			for(InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
				InetAddress broadcast = interfaceAddress.getBroadcast();
				if (broadcast == null)
					continue;

				res.add(broadcast);
			}
		}

		return res;
	}

	public int getServerPortUDP() {
		return socketUDP.getLocalPort();
	}


	// Closeable

	@Override
	public void close() throws IOException {
		socketUDP.close();

		if(socket != null && !socket.isClosed())
			socket.close();

		if(heartbeatThread != null && heartbeatThread.isAlive())
			heartbeatThread.interrupt();

		executorListenerUDP.shutdownNow();
		executorListenerTCP.shutdownNow();
		executorListenerTCP = Executors.newSingleThreadExecutor();
		executorSender = Executors.newSingleThreadExecutor();
	}
}
