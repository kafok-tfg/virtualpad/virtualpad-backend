package virtualpad.backend;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StorageComponent {

	private static Map<String, String> imageMimeTypes;
	static {
		imageMimeTypes = new HashMap<>();
		imageMimeTypes.put("image/bmp", "bmp");
		imageMimeTypes.put("image/gif", "gif");
		imageMimeTypes.put("image/jpe", "jpeg");
		imageMimeTypes.put("image/jpeg", "jpeg");
		imageMimeTypes.put("image/jpg", "jpeg");
		imageMimeTypes.put("image/svg+xml", "svg");
		imageMimeTypes.put("image/tiff", "tiff");
		imageMimeTypes.put("image/x-icon", "ico");
		imageMimeTypes.put("image/png", "png");
		imageMimeTypes.put("image/vnd.wap.wbmp", "wbmp");
	}

	private static String inverseMimeType(String extension) {
		for(Map.Entry<String, String> pair : imageMimeTypes.entrySet())
			if(extension.equals(pair.getValue()))
				return pair.getKey();

		return null;
	}


	// API

	public static String readBucket(Context ctx, String appName, String bucket, boolean isGlobal) throws IOException {
		File path = ctx.getFilesDir();

		String domain;
		if(isGlobal) {
			domain = "global/";
			appName = "";
		} else {
			domain = "apps/";
			appName = (appName.hashCode() + "/").replace("-", "N");
		}
		File file = new File(path, domain + appName + bucket);

		byte[] bytes = new byte[(int) file.length()];

		FileInputStream in = new FileInputStream(file);
		try {
			in.read(bytes);
		} catch (IOException e) {
			throw e;
		} finally {
			in.close();
		}

		return new String(bytes, "UTF-8");
	}

	public static void deleteBucket(Context ctx, String appName, String bucket, boolean isGlobal) throws IOException {
		File path = ctx.getFilesDir();

		String domain;
		if(isGlobal) {
			domain = "global/";
			appName = "";
		} else {
			domain = "apps/";
			appName = (appName.hashCode() + "/").replace("-", "N");
		}
		File file = new File(path, domain + appName + bucket);
		if(!file.delete())
			throw new IllegalArgumentException("Cannot delete bucket '" + bucket +  "'.");

		File[] files = file.listFiles();
		if(files == null || files.length == 0)
			file.delete();
	}

	public static void writeBucket(Context ctx, Host host, String appName, String bucket, String content, boolean isGlobal) throws IOException {
		File path = ctx.getFilesDir();

		String domain;
		String appNameTemp = appName;
		if(isGlobal) {
			domain = "global";
			appName = "";
		} else {
			domain = "apps";
			appName = ("" + appName.hashCode()).replace("-", "N");
		}
		File file = new File(path, domain + "/" + appName + "/" + bucket);

		boolean isNew = new File(path, domain + "/" + appName).mkdirs();

		// Save icon of application
		if(isNew && !isGlobal) {
			File icon = new File(path, domain + "/" + appNameTemp + "." + (host.getIcon() != null ? imageMimeTypes.get(host.getIconMimeType()) : "png"));

			FileOutputStream iconOut = new FileOutputStream(icon);
			try {
				if (host.getIcon() != null)
					iconOut.write(host.getIcon());
			} catch (IOException e) {
				throw e;
			} finally {
				iconOut.close();
			}
		}

		// Save bucket
		FileOutputStream out = new FileOutputStream(file);
		try {
			out.write(content.getBytes("UTF-8"));
		} catch (IOException e) {
			throw e;
		} finally {
			out.close();
		}
	}

	public static String[] listBuckets(Context ctx, String appName, boolean isGlobal) throws IOException {
		File path = ctx.getFilesDir();

		String domain;
		if(isGlobal) {
			domain = "global/";
			appName = "";
		} else {
			domain = "apps/";
			appName = (appName.hashCode() + "/").replace("-", "N");
		}
		File file = new File(path, domain + appName);

		return file.list();
	}


	// Manage

	public static List<Map<String, Object>> getDataInformation(Context ctx) throws IOException {
		List<Map<String, Object>> res = new LinkedList<>();

		File path = new File(ctx.getFilesDir(), "apps");
		File[] filesArray = path.listFiles();
		List<File> files = new LinkedList<>();
		if(filesArray != null)
			for(File file : filesArray)
				files.add(file);

		StringBuilder calculatedSize = new StringBuilder();
		StringBuilder measure = new StringBuilder();
		File globalFile = new File(ctx.getFilesDir(), "global");
		if(globalFile.exists()) {
			calculateSize(globalFile.length(), calculatedSize, measure);

			Map<String, Object> global = new HashMap<>();
			global.put("name", "Global");
			global.put("size", calculatedSize.toString());
			global.put("measure", measure.toString());
			global.put("lastModified", globalFile.lastModified());

			res.add(global);
		}

		for(File app : files) {
			if(!app.isFile())
				continue;

			Map<String, Object> map = new HashMap<>();

			int index = app.getName().lastIndexOf(".");
			String extension = app.getName().substring(index+1);
			String name = app.getName().substring(0, index);

			long size = app.length() + (new File(app.getAbsolutePath().replace(extension, ""))).length();
			calculateSize(size, calculatedSize, measure);

			byte[] icon = null;
			if(size > 0) {
				icon = new byte[(int) size];
				FileInputStream in = new FileInputStream(app);
				try {
					in.read(icon);
				} catch (IOException e) {
					throw e;
				} finally {
					in.close();
				}
			}

			if(icon != null) {
				map.put("icon", icon);
				map.put("iconMimeType", inverseMimeType(extension));
			}

			map.put("name", name);
			map.put("size", calculatedSize.toString());
			map.put("measure", measure.toString());
			map.put("lastModified", app.lastModified());

			res.add(map);
		}

		return res;
	}

	private static void calculateSize(long size, StringBuilder calculatedSize, StringBuilder measure) {
		calculatedSize.setLength(0);
		calculatedSize.append(size/1024.);
		measure.setLength(0);
		measure.append("Kb");
		if(size > 1048576) {
			measure.setLength(0);
			measure.append("Mb");
			calculatedSize.append(size/1048576.);
			if(size > 1048576*1024) {
				measure.setLength(0);
				measure.append("Gb");
				calculatedSize.setLength(0);
				calculatedSize.append(size/(1048576*1024.)/1048576.);
			}
		}
	}

	public static void deleteData(Context ctx, String appName) throws IOException {
		if("Global".equals(appName)) {
			File global = new File(ctx.getFilesDir(), "global");
			if(global.exists()) {
				File[] files = global.listFiles();
				if(files != null)
					for(File file : files)
						file.delete();

				global.delete();
			}

		} else {
			File path = new File(ctx.getFilesDir(), "apps");
			File[] filesArray = path.listFiles();
			List<File> files = new LinkedList<>();
			if(filesArray != null)
				for(File file : filesArray)
					files.add(file);

			for(File app : files) {
				if(!app.isFile())
					continue;

				int index = app.getName().lastIndexOf(".");
				String extension = app.getName().substring(index+1);
				String name = app.getName().substring(0, index);

				if(name.equals(appName))
					app.delete();
			}

			File dir = new File(path, (""+appName.hashCode()).replace("-", "N"));
			if(dir.exists()) {
				File[] buckets = dir.listFiles();
				if(buckets != null)
					for(File bucket : buckets)
						bucket.delete();

				dir.delete();
			}
		}
	}
}
