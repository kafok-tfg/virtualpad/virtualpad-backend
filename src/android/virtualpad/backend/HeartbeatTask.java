package virtualpad.backend;

public class HeartbeatTask implements Runnable {

	private int timeout;
	private Protocol protocol;
	
	
	public HeartbeatTask(int timeout, Protocol protocol) {
		super();
		this.timeout = (int) Math.round(timeout*0.75);
		this.protocol = protocol;
	}


	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(timeout);
				protocol.heartbeat();
			} catch (InterruptedException e) {
				return;
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}

}
