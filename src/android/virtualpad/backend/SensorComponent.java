package virtualpad.backend;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.BatteryManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import virtualpad.backend.constants.Sensors;

public class SensorComponent implements SensorEventListener {

	private static final Map<Integer, Integer> mapSensors;
	private static final Map<Integer, Integer> mapSensorsReverse;
	static {
		mapSensors = new HashMap<>();
		mapSensorsReverse = new HashMap<>();

		mapSensors.put(Sensors.ACCELEROMETER, Sensor.TYPE_ACCELEROMETER);
		mapSensors.put(Sensors.GRAVITY, Sensor.TYPE_GRAVITY);
		mapSensors.put(Sensors.GYROSCOPE, Sensor.TYPE_GYROSCOPE);
		mapSensors.put(Sensors.LIGHT, Sensor.TYPE_LIGHT);
		mapSensors.put(Sensors.LINEAR_ACCELERATION, Sensor.TYPE_LINEAR_ACCELERATION);
		mapSensors.put(Sensors.MAGNETIC_FIELD, Sensor.TYPE_MAGNETIC_FIELD);

		mapSensorsReverse.put(Sensor.TYPE_ACCELEROMETER, Sensors.ACCELEROMETER);
		mapSensorsReverse.put(Sensor.TYPE_GRAVITY, Sensors.GRAVITY);
		mapSensorsReverse.put(Sensor.TYPE_GYROSCOPE, Sensors.GYROSCOPE);
		mapSensorsReverse.put(Sensor.TYPE_LIGHT, Sensors.LIGHT);
		mapSensorsReverse.put(Sensor.TYPE_LINEAR_ACCELERATION, Sensors.LINEAR_ACCELERATION);
		mapSensorsReverse.put(Sensor.TYPE_MAGNETIC_FIELD, Sensors.MAGNETIC_FIELD);
	}

	private Protocol protocol;

	private Vibrator vibrator;
	private SensorManager sensorManager;

	private Map<Integer, Sensor> sensors;
	private List<Integer> measurements;


	public SensorComponent(Protocol protocol) {
		super();
		this.protocol = protocol;
		this.sensors = new HashMap<>();
		this.measurements = new LinkedList<>();

		Context ctx = protocol.getPlugin().cordova.getActivity().getApplicationContext();
		this.sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);

		Sensor temp = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (temp != null)
			this.sensors.put(Sensors.ACCELEROMETER, temp);

		temp = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		if (temp != null)
			this.sensors.put(Sensors.GRAVITY, temp);

		temp = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		if (temp != null)
			this.sensors.put(Sensors.GYROSCOPE, temp);

		temp = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (temp != null)
			this.sensors.put(Sensors.LIGHT, temp);

		temp = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		if (temp != null)
			this.sensors.put(Sensors.LINEAR_ACCELERATION, temp);

		temp = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		if (temp != null)
			this.sensors.put(Sensors.MAGNETIC_FIELD, temp);

		PackageManager packageManager = ctx.getPackageManager();

		if (packageManager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE))
			this.sensors.put(Sensors.MICROPHONE, null);

		if (packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_OUTPUT))
			this.sensors.put(Sensors.SPEAKER, null);

		if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
			this.sensors.put(Sensors.CAMERA_FRONT, null);

		if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
			this.sensors.put(Sensors.CAMERA, null);

		this.vibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
		if (this.vibrator.hasVibrator())
			this.sensors.put(Sensors.VIBRATION, null);

		this.sensors.put(Sensors.KEYBOARD, null);
	}

	public List<Integer> getSensors() {
		return new LinkedList<Integer>(sensors.keySet());
	}

	public boolean onData(int idSensor, ByteBuffer buffer) {
		boolean res = true;

		switch (idSensor) {
			case Sensors.SPEAKER:
				ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
				tone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, buffer.getInt());
				break;

			case Sensors.VIBRATION:
				if (this.sensors.containsKey(Sensors.VIBRATION)) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
						vibrator.vibrate(VibrationEffect.createOneShot(buffer.getInt(), VibrationEffect.DEFAULT_AMPLITUDE));
					else // deprecated in API 26
						vibrator.vibrate(buffer.getInt());
				}

				break;

			default:
				res = false;
				break;
		}

		return res;
	}


	public void measure(List<Integer> measurements, VirtualPadBackend plugin) {
		this.measurements = measurements;
		start();
	}

	public void start() {
		if(startedSensorUsed < 0)
			startedSensorUsed = System.currentTimeMillis();

		stop();
		for(int sensor : measurements)
			sensorManager.registerListener(this, sensors.get(sensor), SensorManager.SENSOR_DELAY_GAME);
	}

	public void stop() {
		sensorManager.unregisterListener(this);

		if(startedSensorUsed > 0) {
			sensorUsedTime += (System.currentTimeMillis() - startedSensorUsed);
			startedSensorUsed = -1;
		}
	}


	@Override
	public final void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	@Override
	public final void onSensorChanged(SensorEvent event) {
		int idSensor = mapSensorsReverse.get(event.sensor.getType());
		try {
			protocol.sendData(idSensor, event.values);
		} catch(Exception e) {}
	}


	// Statistics

	private long startedSession;
	private int startedBattery;
	private long startedSensorUsed;
	private long sensorUsedTime;
	private int initialBattery;
	private int endBattery;

	public void startSession() {
		startedSession = System.currentTimeMillis();
		sensorUsedTime = 0;
		startedSensorUsed = -1;

		Context ctx = protocol.getPlugin().cordova.getActivity().getApplicationContext();
		BatteryManager bm = (BatteryManager) ctx.getSystemService(Context.BATTERY_SERVICE);
		startedBattery = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER);
		initialBattery = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
	}

	public String stopSession() {
		if(startedSensorUsed > 0)
			sensorUsedTime += (System.currentTimeMillis() - startedSensorUsed);

		Context ctx = protocol.getPlugin().cordova.getActivity().getApplicationContext();
		BatteryManager bm = (BatteryManager) ctx.getSystemService(Context.BATTERY_SERVICE);
		endBattery = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
		return "{" +
			"\"duration\": " + (System.currentTimeMillis() - startedSession) + "," +
			"\"use-of-sensord\": " + sensorUsedTime + "," +
			"\"initial-battery\": " + initialBattery + "," +
			"\"end-battery\": " + endBattery + "," +
			"\"use-of-locks\": LOCK_USED" +
		"}";
	}
}
