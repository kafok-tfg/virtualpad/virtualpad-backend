package virtualpad.backend;

public class Host {

	private String addr;
	private int udpPort;
	private int tcpPort;
	private String name;
	private int latency;
	private byte[] icon;
	private String iconMimeType;


	public Host() {
		super();
	}


	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public int getUdpPort() {
		return udpPort;
	}

	public void setUdpPort(int udpPort) {
		this.udpPort = udpPort;
	}

	public int getTcpPort() {
		return tcpPort;
	}

	public void setTcpPort(int tcpPort) {
		this.tcpPort = tcpPort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLatency() {
		return latency;
	}

	public void setLatency(int latency) {
		this.latency = latency;
	}

	public byte[] getIcon() {
		return icon;
	}

	public void setIcon(byte[] icon) {
		this.icon = icon;
	}

	public String getIconMimeType() {
		return iconMimeType;
	}

	public void setIconMimeType(String iconMimeType) {
		this.iconMimeType = iconMimeType;
	}

	public String toString() {
		return "{"
			+ "\"name\": \"" + name + "\""
			+ ", \"addr\": \"" + addr + "\""
			+ ", \"udpPort\": " + udpPort
			+ ", \"tcpPort\": " + tcpPort
			+ ", \"latency\": \"" + latency + "\""
		+ "}";
	}
}
